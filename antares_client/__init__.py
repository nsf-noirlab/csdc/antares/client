__version__ = "v1.8.0"

from . import search
from .exceptions import AntaresException
from .stream import StreamingClient
