.. _api:

API Reference
=============

.. module:: antares_client

Streaming Client
----------------

.. autoclass:: StreamingClient
   :members:
   :inherited-members:

Search
------

.. automodule:: antares_client.search
   :members:

Models
------

.. automodule:: antares_client.models
   :members:
