User's Guide
------------

.. toctree::
   :maxdepth: 2

   installation
   tutorial
   troubleshooting

API Reference
-------------

.. toctree::
   :maxdepth: 2

   api

Additional Notes
----------------

.. toctree::
   :maxdepth: 2

   acknowledgements
   changelog
   license
